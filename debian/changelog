libhandy (0.0.13-3) unstable; urgency=medium

  [ Guido Günther ]
  * [ca8d3c6] d/control: Switch maintainer to team address
  * [e9f4f34] Disable glade catalog.
    This avoids problems when libhandy-1's glade support is
    around too and unbreaks glade 3.38. (Closes: #977187)

  [ Federico Ceratto ]
  * [72e1d2c] Trim trailing whitespace.
    Fixes: lintian: file-contains-trailing-whitespace
  * [581917e] Set upstream metadata fields: Repository.
    Fixes: lintian: upstream-metadata-file-is-missing
  * [35d489d] Bump debhelper compat version

 -- Guido Günther <agx@sigxcpu.org>  Sat, 12 Dec 2020 13:44:55 +0100

libhandy (0.0.13-2) unstable; urgency=medium

  * [0fd724f] Disable atk during tests.
    This fixes test problems on GNU Hurd.
    Thanks to Samuel Thibault for investigating
    (Closes: #953971)

 -- Guido Günther <agx@sigxcpu.org>  Wed, 25 Mar 2020 10:14:43 +0100

libhandy (0.0.13-1) unstable; urgency=medium

  [ Guido Günther ]
  * New upstream version 0.0.13

  [ Sebastien Bacher ]
  * [4c770b6] Update the build test to closer from other GNOME ones.
    Fix some issues/the current test failing
      - don't use a deprecated function
      - handle cross build autopkgtest environment
      - include a runtime test as well (Closes: #946676)

 -- Guido Günther <agx@sigxcpu.org>  Fri, 27 Dec 2019 12:22:18 +0100

libhandy (0.0.12-1) unstable; urgency=medium

  * New upstream version 0.0.12

 -- Guido Günther <agx@sigxcpu.org>  Thu, 12 Dec 2019 09:49:04 +0100

libhandy (0.0.11-1) unstable; urgency=medium

  * New upstream version 0.0.11

 -- Guido Günther <agx@sigxcpu.org>  Sat, 07 Sep 2019 01:11:28 -0700

libhandy (0.0.10-1) unstable; urgency=medium

  * New upstream version 0.0.10

 -- Guido Günther <agx@sigxcpu.org>  Tue, 18 Jun 2019 15:04:53 +0200

libhandy (0.0.9-1) unstable; urgency=medium

  * New upstream version 0.0.9 (Closes: #924754)

 -- Guido Günther <agx@sigxcpu.org>  Mon, 18 Mar 2019 13:27:03 +0100

libhandy (0.0.8-1) unstable; urgency=medium

  [Guido Günther]
  * New upstream version 0.0.7

  [ Jeremy Bicha ]
  * [935807c] Build-Depend on debhelper-compat 12 and drop debian/compat
    debhelper compat 12 uses dh_makeshlibs -VUpstream-Version by default
  * [39272b8] Build-Depend on dh-sequence-gir
    dh-sequence-gir is a virtual package provided by gobject-introspection.
    debhelper 12 automatically enables the gir helper when this is
    part of Build-Depends.
    https://manpages.debian.org/unstable/dh#OPTIONS
  * [e3cb678] Drop gir1.2-handy-0.0's unused dependency on ${shlibs:Depends}
  * [20ef4cd] Build-Depend on libglib2.0-doc & libgtk-3-doc to fix doc build
    warnings

 -- Guido Günther <agx@sigxcpu.org>  Fri, 15 Feb 2019 11:27:35 +0100

libhandy (0.0.7-1) unstable; urgency=medium

  * New upstream version 0.0.7

 -- Guido Günther <agx@sigxcpu.org>  Mon, 21 Jan 2019 18:50:45 +0100

libhandy (0.0.6-1) unstable; urgency=medium

  * New upstream version 0.0.6

 -- Guido Günther <agx@sigxcpu.org>  Mon, 17 Dec 2018 17:09:36 +0100

libhandy (0.0.5-2) unstable; urgency=medium

  [ Jeremy Bicha ]
  * [8eb01dc] Add Vcs fields

  [ Guido Günther ]
  * Upload to unstable
  * [9964c77] Add gitlab-ci.
    Thanks salsa-ci-team
  * [37c5b11] hdy-enums: Make build reproducible
  * [29187af] Add Build-Depends-Package to symbols file.
    This ensures generated dependencies are at least as strong as the used
    build-dep.

 -- Guido Günther <agx@sigxcpu.org>  Tue, 27 Nov 2018 12:00:56 +0100

libhandy (0.0.5-1) experimental; urgency=medium

  * New upstream version

 -- Guido Günther <agx@sigxcpu.org>  Mon, 12 Nov 2018 08:51:01 +0100

libhandy (0.0.4-2) unstable; urgency=medium

  [ Guido Günther ]
  * [9651d62] meson: Properly depend on the generated headers
    Fixes FTBFS.
  * [c48897d] d/control: Mark buil-deps for tests as <!nocheck>

  [ Jeremy Bicha ]
  * [84d9e5c] Have libhandy-0.0-dev depend on libgtk-3-dev (Closes: #910384)
  * [b3ea207] Use dh --with gir
  * [3a82073] Simplify debian/rules

 -- Guido Günther <agx@sigxcpu.org>  Sat, 06 Oct 2018 14:25:32 +0200

libhandy (0.0.4-1) unstable; urgency=medium

  * New upstream version

 -- Guido Günther <agx@sigxcpu.org>  Fri, 05 Oct 2018 19:27:24 +0200

libhandy (0.0.3-1) unstable; urgency=medium

  * Upload to unstable
  * New upstream version
    - New widget HdyTitleBar
    - Lots of fixes
  * Update symbols file

 -- Guido Günther <agx@sigxcpu.org>  Tue, 18 Sep 2018 09:41:38 +0200

libhandy (0.0.2-1) experimental; urgency=medium

  * New upstream version

 -- Guido Günther <agx@sigxcpu.org>  Sun, 09 Sep 2018 18:08:59 +0200

libhandy (0.0.1-1) experimental; urgency=medium

  * Initial upload to experimental (Closes: #901509)

 -- Guido Günther <agx@sigxcpu.org>  Sat, 09 Jun 2018 09:12:06 +0200
